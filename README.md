# bigbang-test-backend

## Getting Started

First, run the development bakcend server to provide API endpoint:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

API url of getting users' names stored in JSON file (data.json) in backend server is [http://localhost:5000/api/users](http://localhost:5000/api/users) with your browser to see the result.

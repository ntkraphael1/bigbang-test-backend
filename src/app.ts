import express, { Router } from "express";
import dotenv from "dotenv";
import { User } from "./userType";
import * as fs from "fs";
import cors from "cors";

dotenv.config();

express.json();
express.urlencoded({extended: true});

const app = express();
const PORT = process.env["PORT"];

app.use(cors({
  origin: "*"
}))

app.get("/api/users", (_, res) => {
    try {
        const data = JSON.parse(fs.readFileSync("./src/data.json", "utf-8"));
        if ("users" in data) {
            console.log(data);
            const users = data["users"] satisfies User[];
            res.status(200).json(users);
        }
      } catch (err) {
        console.error(err);
      };    
});

// app.post("api/users", (req, res) => {
//     const { id, name }: User = req.body;
//     const newUser = { id, name };
//     res.json(newUser);
// })

app.listen(PORT, () => {
    console.log(`API server listening on port ${PORT}`);
})
